// (Don't put {} around the anynonymous functions' actions in fetch().then(); it breaks the code. You can use multiline without {}, and it will work.)

// fetch() method is JavaScript is used to request the server and load the information in the webpages. The request can be of nay APIs that return the data of the format JSON.

// Syntax: fetch('url', options)
	// url - This is the url to which to request is to be made.
	// options - an array of properties. It is an optional parameter.

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data));

// Show Posts
const showPosts = (posts)  => {
	let postEntries = '';

	posts.forEach((post) => {

		console.log(post);

		postEntries += `
		<div id="post-${post.id}">

			<h3 id="post-title-${post.id}">${post.title}</h3>

			<p id="post-body-${post.id}">${post.body}</p>

			<button onclick="editPost('${post.id}')">Edit</button>

			<button onclick="deletePost('${post.id}')">Delete</button>

		</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Add Post
document.querySelector('#form-add-post')
.addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {

		method : 'POST',
		body : JSON.stringify({
			title : document.querySelector("#txt-title").value,
			body : document.querySelector("#txt-body").value,
			userId : 1
		}),
		headers : {'Content-Type' : 'application/json'}
	})
	.then((response) => response.json())
	.then((data) => {

		console.log(data);
		alert('Post Successfully Added!');

		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;

		// (This doesn't work because a placeholder is used.)
		// showPosts(data);
	});
});

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;

	document.querySelector("#txt-edit-title").value = title;

	document.querySelector("#txt-edit-body").value = body;

	document.querySelector("#btn-submit-update")
	.removeAttribute('disabled');
};

// Update Post
document.querySelector("#form-edit-post")
.addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method : "PUT",
		body : JSON.stringify({
			id : document.querySelector("#txt-edit-id").value,
			title : document.querySelector("#txt-edit-title").value,
			body : document.querySelector("#txt-edit-body").value,
			userId : 1
		}),
		headers : {"Content-Type" : "application/json"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post Successfully Updated!");

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector("#btn-submit-update")
		.setAttribute('disabled', true);
	})
});

// A C T I V I T Y

// Delete Post
const deletePost = (id) => {

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method : "DELETE"
	});

	document.querySelector(`#post-${id}`).remove()
};